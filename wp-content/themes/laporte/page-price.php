<?php get_header(); ?>

    <?php
    global $post;
    $slug = $post->post_name;
    ?>

    <section class="common_content">
        <div class="common_content_wrapper">
            <div class="section_ttl">
                <h1><?php echo get_the_title(); ?></h1>
                <span class="sub_ttl"><?php echo ucfirst($slug); ?></span>
            </div>
            <div class="common_content_container">
                <p class="center sp_left mb_40">
                    全室、サービス料込み
                </p> 
                <div class="tab-container mb_40">
                    <div class="tab-wrap">
                        <ul>
                            <li class="tab-btn show"><a href="#price01"><strong>ショートタイム</strong><br>100分</a></li>
                            <li class="tab-btn"><a href="#price02"><strong>ご休憩</strong><br>最大3時間</a></li>
                            <li class="tab-btn"><a href="#price03"><strong>サービスタイム</strong><br>最大8時間</a></li>
                            <li class="tab-btn one-row"><a href="#price04"><strong>ご宿泊</strong></a></li>
                            <li class="tab-btn"><a href="#price05"><strong>深夜休憩</strong><br>最大3時間</a></li>
                            <li class="tab-btn"><a href="#price06"><strong>ラポルテの日</strong><br>毎週木曜日</a></li>
                            <li class="tab-btn one-row"><a href="#price07"><strong>ご延長</strong></a></li>
                        </ul>
                        <div class="tab-contents show" id="price01">
                            <div class="plan_type_text">
                                <h2>ショートタイム</h2>
                                <p class="center size20 mb_12">最大100分</p>
                                <p class="center size20">5時〜18時の間にInで100分</p>
                            </div>
                            
                            <div class="dis_fl bet plan_type">
                                <div class="plan_type_item">
                                    <h3>A type</h3>
                                    <p>
                                        平日 ¥3,270<br>
                                        土・日・祝日 ¥3,820
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>B type</h3>
                                    <p>
                                        平日 ¥3,710<br>
                                        土・日・祝日 ¥4,260
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>C type</h3>
                                    <p>
                                        平日 ¥4,150<br>
                                        土・日・祝日 ¥4,560
                                    </p>
                                </div>
                            </div>

                            <div class="center">
                                メンバー様限定<span class="size24 ml_12">2ドリンク無料</span>
                            </div>
                        </div>
                        <div class="tab-contents" id="price02">
                            <div class="plan_type_text">
                                <h2>ご休憩</h2>
                                <p class="center size20 mb_12">最大3時間</p>
                                <p class="center size20">5時〜21時の間にInで3時間</p>
                            </div>
                            
                            <div class="dis_fl bet plan_type">
                                <div class="plan_type_item">
                                    <h3>A type</h3>
                                    <p>
                                        平日 ¥4,040<br>
                                        土・日・祝日 ¥4,590
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>B type</h3>
                                    <p>
                                        平日 ¥4,370<br>
                                        土・日・祝日 ¥4,920
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>C type</h3>
                                    <p>
                                        平日 ¥4,810<br>
                                        土・日・祝日 ¥5,360
                                    </p>
                                </div>
                            </div>

                            <div class="center">
                                メンバー様限定<span class="size24 ml_12">2ドリンク・2フード無料</span>
                            </div>
                        </div>
                        <div class="tab-contents" id="price03">
                            <div class="plan_type_text">
                                <h2>サービスタイム</h2>
                                <p class="center size20 mb_12">最大8時間</p>
                                <p class="center size20 mb_12">5:01〜11:00の間にInで8時間</p>
                                <p class="center size20 mb_12">11:01〜15:00の間にInで6時間</p>
                                <p class="center size20 mb_12">15:01〜19:00の間にInで4時間</p>
                                <p class="center size20 mb_12">土・日・祝日は無し</p>
                                <p class="center">超過時間の場合は延長料金が発生します。</p>
                            </div>
                            
                            <div class="dis_fl bet plan_type">
                                <div class="plan_type_item">
                                    <h3>A type</h3>
                                    <p>
                                    ¥4,590
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>B type</h3>
                                    <p>
                                    ¥5,030
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>C type</h3>
                                    <p>
                                    ¥5,250
                                    </p>
                                </div>
                            </div>

                            <div class="center">
                                メンバー様限定<span class="size24 ml_12">2ドリンク・2フード無料</span>
                            </div>
                        </div>
                        <div class="tab-contents" id="price04">
                            <div class="plan_type_text">
                                <h2>ご宿泊</h2>
                                <p class="center size20 mb_12">21時in〜11時out</p>
                                <p class="center">11時を過ぎますと延長料金が発生します。</p>
                            </div>
                            
                            <div class="dis_fl bet plan_type">
                                <div class="plan_type_item">
                                    <h3>A type</h3>
                                    <p>
                                        平日 ¥6,570<br>
                                        土・日・祝日 ¥7,670
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>B type</h3>
                                    <p>
                                        平日 ¥7,340<br>
                                        土・日・祝日 ¥8,440
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>C type</h3>
                                    <p>
                                        平日 ¥8,550<br>
                                        土・日・祝日 ¥9,650
                                    </p>
                                </div>
                            </div>

                            <div class="center">
                                メンバー様限定<span class="size24 ml_12">2ドリンク・2フード無料</span>
                            </div>
                        </div>
                        <div class="tab-contents" id="price05">
                            <div class="plan_type_text">
                                <h2>深夜休憩</h2>
                                <p class="center size20 mb_12">最大3時間</p>
                                <p class="center size20 mb_12">21時〜24時の間にInで3時間</p>
                                <p class="center size20 mb_12">土・日・祝日は無し</p>
                                <p class="center">ご利用3時間を過ぎますと宿泊料金に変わります。</p>
                            </div>
                            
                            <div class="dis_fl bet plan_type">
                                <div class="plan_type_item">
                                    <h3>A type</h3>
                                    <p>
                                    ¥4,590
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>B type</h3>
                                    <p>
                                    ¥5,030
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>C type</h3>
                                    <p>
                                    ¥5,470
                                    </p>
                                </div>
                            </div>

                            <div class="center">
                                メンバー様限定<span class="size24 ml_12">2ドリンク・2フード無料</span>
                            </div>
                        </div>

                        <div class="tab-contents" id="price06">
                            <div class="plan_type_text">
                                <h2>ラポルテの日</h2>
                                <p class="center size20 mb_12">毎週木曜</p>
                                <p class="center size20">5時〜22時30分の間にInで90分</p>
                            </div>
                            
                            <div class="dis_fl bet plan_type">
                                <div class="plan_type_item">
                                    <h3>A type</h3>
                                    <p>
                                    ¥2,170
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>B type</h3>
                                    <p>
                                    ¥2,170
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>C type</h3>
                                    <p>
                                    ¥2,170
                                    </p>
                                </div>
                            </div>

                        </div>

                        <div class="tab-contents" id="price07">
                            <div class="plan_type_text">
                                <h2>ご延長</h2>
                                <p class="center size20">30分毎</p>
                            </div>
                            
                            <div class="dis_fl bet plan_type">
                                <div class="plan_type_item">
                                    <h3>A type</h3>
                                    <p>
                                        平日 ¥880<br>
                                        土・日・祝日 ¥990
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>B type</h3>
                                    <p>
                                        平日 ¥990<br>
                                        土・日・祝日 ¥1,100
                                    </p>
                                </div>
                                <div class="plan_type_item">
                                    <h3>C type</h3>
                                    <p>
                                        平日 ¥1,100<br>
                                        土・日・祝日 ¥1,210
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div>
                    <p class="mb_20">
                        ※24:00を過ぎると宿泊または深夜休憩料金に変ります。（21時以前のご滞在分は前延長料金として加算させて頂きます。） <br>
                        ※深夜休憩はトータルで3時間以内です、3時間を過ぎると宿泊になりますのでご注意ください。 <br>
                        ※サービスタイム・深夜休憩は土・日・祝日はご利用になれません。 <br>
                        ※上記料金は1・2名様対象です。追加人数料金はお一人様につき50%UPとなります。 <br>
                        ※日曜と祝前日が重なる場合は祝前日が優先となります。 <br>
                        ※特別期間は上記の価格表と異なります。（年末年始・GW・お盆・イベント期間等） <br>
                        ※チェックアウト精算後は5分以内に御退出ください。 <br>
                    </p>
                    <p>
                        電話でのご予約が可能です。詳しくはホテルにご確認ください。<br>
                        TEL 0773-58-2226 （ご予約いただきましたお客さまには素敵なプレゼントをご用意させていただいております。） <br>
                        お部屋のご指定は可能ですが、ご予約済み、ご利用中等の場合によっては指定が出来ない場合もございます。 <br>
                        なおご予約のお時間を１5分経過しましたらキャンセルの扱いとさせていただきますのでご了承ください。 <br>
                        掲載の料金は全て税込価格です。
                    </p>

                </div>
            </div>
        </div>
    </section>
    
<?php get_footer(); ?>
