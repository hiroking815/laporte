　
        </main>

        <?php
        if(!is_home()): ?>
        <div class="bread" itemscope itemtype="http://schema.org/BreadcrumbList">
            <div class="bread_wrap" >
                <?php breadcrumb();
                ?>

            </div>

        </div>
        <? 
        endif;?>
        <footer>
            <div class="footer_inner">
                <div class="footer_links common_content_container dis_fl bet">
                    <div class="w_240 sp_w_100per sp_mb_20">
                        <p class="f_ttl"><?php echo get_bloginfo('name'); ?></p>
                        <p>
                            〒620-1421 <br>京都府福知山市三和町大身348
                        </p>
                        <p>TEL <a href="tel:0773582226">0773-58-2226</a></p>
                        <a class="logo_link" href="/">
                            <img src="<?php echo img_dir().'common/logo.png'; ?>" class="logo_txt">
                        </a>
                    </div>
                    <div class="w_400 sp_w_100per sp_mb_20">
                        <p class="f_ttl">サイトマップ</p>
                        <div class="dis_fl bet sitemap">

                            <?php 
                            wp_nav_menu( array( 
                                'menu_class' => 'w_200',
                                'theme_location' => 'footer-sitemap-left' 
                            ) ); 
                            ?>
                            <?php 
                            wp_nav_menu( array( 
                                'menu_class' => 'w_200',
                                'theme_location' => 'footer-sitemap-right' 
                            ) ); 
                            ?>
                        </div>
                    </div>
                    <div class="w_240 sp_w_100per sp_mb_20">
                        <p class="f_ttl">スタッフ募集中</p>
                        <p>
                            一緒に働いていただけるスタッフを募集しております！ <br>
                            【仕事】 ルーム清掃兼フロント補助（未経験者も大歓迎！）<br>
                            詳しくはお気軽に電話にてお問い合わせください。<br>
                            TEL <a href="tel:0773582226">0773-58-2226</a>
                        </p>
                    </div>
                </div>
                
            </div>
            <div class="copyright">
                ©︎ <?php echo date('Y'); ?> <?php echo get_bloginfo(); ?>
            </div>

            
        </footer>
        

    </body>

</html>