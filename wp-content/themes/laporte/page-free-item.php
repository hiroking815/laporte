<?php get_header(); ?>

    <?php
    global $post;
    $slug = $post->post_name;

    $free_item_imgs = [
        img_dir().'common/free_item01.jpg',
        img_dir().'common/free_item02.jpg'
    ];
    ?>
    <section class="common_content">
        <div class="common_content_wrapper">
            <div class="section_ttl">
                <h1><?php echo get_the_title(); ?></h1>
                <span class="sub_ttl"><?php echo ucfirst($slug); ?></span>
            </div>
            <div class="common_content_container">
                <?php 
                foreach($free_item_imgs as $img_path): ?>

                    <a class="free_item mb_40" href="<?php echo $img_path; ?>" target="_blank">
                        <img src="<?php echo $img_path; ?>" href="<?php echo $img_path; ?>" target="_blank" rel="noopener norefferer">
                    </a> 
                <?php 
                endforeach; ?>

            </div>
        </div>
    </section>

    

<?php get_footer(); ?>
