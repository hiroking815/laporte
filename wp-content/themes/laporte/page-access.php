<?php get_header(); ?>

    <?php
    global $post;
    $slug = $post->post_name;
    ?>
    <section class="common_content">
        <div class="common_content_wrapper">
            <div class="section_ttl">
                <h1><?php echo get_the_title(); ?></h1>
                <span class="sub_ttl"><?php echo ucfirst($slug); ?></span>
            </div>
            <div class="common_content_container">
                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3260.255608300178!2d135.28021961524578!3d35.200101580309266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60001614f1905b8f%3A0x1e0c1da53a4b4316!2z44Op44O744Od44Or44OGIOemj-efpeWxseW6lw!5e0!3m2!1sja!2sjp!4v1636725106671!5m2!1sja!2sjp" width="800" height="600" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
                <div>
                    <ul>
                        <li>舞鶴若狭自動車道「福知山IC」から国道9号線で京都方面へ。 三和町役場より京都方面に車で10分。</li>
                        <li>須地「やまがた屋」ドライブインより福知山方面へ車で10分。</li>
                    </ul>
                    〒620-1421 京都府福知山市三和町大身348<br>
                    TEL. <a href="tel:0773582226">0773-58-2226</a>
                    </p>
                </div> 
            </div>
        </div>
    </section>

    

<?php get_footer(); ?>
