<?php get_header(); ?>
        <section class="normal sp_none"></section>
        <section class="posts">
            <div class="post_inner inner">
                <article class="post_item">
                    <?php 
                    if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
                    elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
                    else { $paged = 1; }  

                    if ( have_posts() ) : ?>
                        <?php 
                        while ( have_posts() ) : 
                            the_post(); 
                            $cat = get_the_category();
                            $cat_name = $cat[0]->cat_name; 
                            $post_date = get_the_date('Y.m.d'); 
                            $cat_slug = $cat[0]->slug;

                            $thumbnail_pass = "";
                            if(has_post_thumbnail()):
                                $thumbnail_pass = get_the_post_thumbnail_url($post->ID, l_t());
                            else:
                                $thumbnail_pass = no_img();
                            endif; ?>
                            <div class="post_item_info">
                                <p class="post_time"><i class="far fa-clock"></i><?php echo $post_date; ?></p>
                                <a href="/<?php echo $cat_slug; ?>" class="post_category"><?php echo $cat_name ?></a>
                            </div>
                            <h1>
                            <?php the_title(); ?>
                            </h1>


                            <img class="thumbnail" src="<?php echo $thumbnail_pass; ?>" alt="<?php the_title(); ?>">
                            <?php the_content(); ?>

                        <?php 
                        endwhile;
                        ?>
                        <div class="post_pagination">
                            <?php 
                            if (get_previous_post()):
                                previous_post_link('%link', '&laquo; %title');
                            endif; ?>
                            <?php 
                            if (get_next_post()):
                                next_post_link('%link', '%title &raquo;'); 
                            endif;?>
                        </div>


                    <?php 
                    else : ?>
                            <p>記事はありません。</p>
                    <?php 
                    endif; 
                    wp_reset_postdata(); ?> 
                
                    

                </article>
                

                
                
            </div>
        </section>

    

<?php get_footer(); ?>
