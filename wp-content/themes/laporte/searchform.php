<form method="get" action="<?php echo home_url('/'); ?>" class="search_container">
    <input type="text" id="s-box" name="s" placeholder="サイト内を検索">
    <label>
        <input class="input_main" type="submit" value="&#xf002">
        <i class="fas fa-search" title="検索する"></i> 
    </label>
</form>