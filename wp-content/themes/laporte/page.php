<?php get_header(); ?>

        <section class="normal center">
        </section>

        <section class="posts">
            <div class="post_inner inner">
                <div class="post_item">
                    <?php while (have_posts()): the_post(); ?>
                    <h1><?php the_title(); ?></h1>
                    <?php the_content(); ?>
                    <?php endwhile; ?>
                    
                </div>
                                
                
            </div>
        </section>

    
<?php get_footer(); ?>
