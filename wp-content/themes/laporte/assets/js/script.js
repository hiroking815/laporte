

jQuery(function($){

    $('.openbtn').on('click', function(){
        $(this).stop().toggleClass('active');

        if($(this).hasClass('active')){
            $('.openbtn .letter').text('CLOSE');
            $('header').css('height','100vh');
        } else {
            $('.openbtn .letter').text('MENU');
            $('header').css('height','auto');

        }

        $('.header_right').stop().slideToggle();
    });

    $('.tab-btn').on('click', function() {
        var tabWrap = $(this).parents('.tab-wrap');
        var tabBtn = tabWrap.find(".tab-btn");
        var tabContents = tabWrap.find('.tab-contents');
        tabBtn.removeClass('show');
        $(this).addClass('show');
        var elmIndex = tabBtn.index(this);
        tabContents.removeClass('show');
        tabContents.eq(elmIndex).addClass('show');
    });
    
});