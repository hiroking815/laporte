<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=yes">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link rel="shortcut icon" href="<?php echo img_dir().'common/logo.png'; ?>">
        <?php wp_head(); ?>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;700&family=Noto+Serif+JP:wght@400;700&display=swap" rel="stylesheet">
        <meta name="google-site-verification" content="oAAJCT-DF6DLtJZx-HnkqemAIhpuBdovDeRuqaKojIo" />
        
    </head>

    <body class="<?php echo bdy_cls(); ?>">
        
        <?php 
        $this_url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
        global $post;
        $this_ttl = get_bloginfo('name');
        if(!is_front_page() || !is_home()):
            $this_ttl = $post->post_title; 
        endif; ?>
        
        <header>
            <div class="h_wrap">
                <div class="header_left">
                    <a class="logo_link" href="/">
                        <img class="logo" src="<?php echo img_dir().'common/logo.png'; ?>">
                    </a>
                    <a class="title_link" href="/">
                        <?php if(is_front_page() || is_home()): ?>
                            <h1><?php echo get_bloginfo('name'); ?></h1>
                        <?php else: ?>
                            <p class="site_title"><?php echo get_bloginfo('name'); ?></p>
                        <?php endif; ?>
                    </a>
                    <div class="openbtn pc_none"><span class="border"></span><span class="border"></span><span class="letter">MENU</span></div>
                </div>

                <nav class="header_right">
                    <!-- <li><a href="/room">お部屋</a></li>
                    <li><a href="/food">食事</a></li>
                    <li><a href="/price">料金</a></li>
                    <li><a href="/member">メンバーズカード</a></li>
                    <li><a href="/access">アクセス</a></li>
                    <li><a href="/contact" class="cv_link">予約サイトへ<i class="fas fa-external-link-alt"></i></a></li> -->
                    <?php 
                    wp_nav_menu( array( 
                        'theme_location' => 'header-nav' 
                    ) ); 
                    ?>
                    <ul><li><a href="https://couples.jp/hotel-details/2300" class="cv_link" target="_blank" rel="noopener noreferrer">予約サイトへ<i class="fas fa-external-link-alt"></i></a></li><ul>

                </nav>
            </div>
        </header>



        <main>