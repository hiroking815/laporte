<?php 

function setup_my_theme() {
    add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'setup_my_theme');


add_post_type_support( 'page', 'excerpt' );

add_theme_support('post-thumbnails');

add_filter('show_admin_bar', '__return_false');


add_action( 'admin_menu', 'remove_menus' );
function remove_menus(){

    remove_menu_page( 'edit-comments.php' );

}



function register_stylesheet() { 
    wp_register_style('reset', get_template_directory_uri().'/assets/css/reset.css');
	wp_register_style('fontawesome', get_template_directory_uri().'/assets/css/all.min.css');
	wp_register_style('theme_style', get_template_directory_uri().'/assets/css/style.css', "", date('Ymd'));
}
function add_stylesheet() { 
	register_stylesheet();
	wp_enqueue_style('reset', '', array(), '1.0', false);
    wp_enqueue_style('fontawesome', '', array(), '1.0', false);
	wp_enqueue_style('theme_style', '', array(), '1.0', false);
}




function register_script(){
	wp_register_script('jquery_js', get_template_directory_uri().'/assets/js/jquery.min.js');
	wp_register_script('fontawesome_js', get_template_directory_uri().'/assets/js/all.min.js');
	wp_register_script('theme_script', get_template_directory_uri().'/assets/js/script.js');

}
function add_script(){ 
	register_script();
	wp_enqueue_script('jquery_js', '', array(), '1.0', false);
	wp_enqueue_script('fontawesome_js', '', array(), '1.0', false);
	wp_enqueue_script('theme_script', '', array(), '1.0', false);

}

add_action('wp_enqueue_scripts', 'add_stylesheet');
if ( ! is_admin() ) {
	add_action('wp_print_scripts','add_script');
} 

add_filter("get_the_content", "wpautop", 0, 1);


function the_pagination() {
	global $wp_query;
	if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
	elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
	else { $paged = 1; } 
	$bignum = 999999999;
	$paginate_args = array(
		'base'         => str_replace( $bignum, '%#%', '?page=%#%' ),
		'format'       => '?page=%#%',
		'current'      => max(0, $paged),
		'total'        => $wp_query->max_num_pages,
		'prev_text'    => '&lt;',
		'next_text'    => '&gt;',
		'type'         => 'list',
		'end_size'     => 1,
		'mid_size'     => 2
	);
	if ( $wp_query->max_num_pages <= 1 ) {
		return;
	}
	echo paginate_links( $paginate_args );
}



// パンくずリスト
function breadcrumb(){
	global $post;
	$str ='';
	$item_prop_content = 0;

	if(!is_home()&&!is_admin()){
		$item_prop_content++;
		// $str.= '<a href="'.home_url().'" class="home" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><i class="fas fa-home" title="home"></i><meta itemprop="position" content="'.strval($item_prop_content).'" /></a> > ';
		$str.= '<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="'.home_url().'" class="home" itemprop="item"><span><i class="fas fa-home" title="home"></i><span class="no_v" style="display:none" itemprop="name">ホーム</span></span></a> > <meta itemprop="position" content="'.strval($item_prop_content).'" /></div>';
		if(is_category()) {
			$cat = get_queried_object();
			if($cat->parent != 0){
				$ancestors = array_reverse(get_ancestors( $cat->cat_ID, 'category' ));
				foreach($ancestors as $ancestor){
					$item_prop_content++;
					$str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">'. get_cat_name($ancestor) .'</span><meta itemprop="position" content="'.strval($item_prop_content).'" /> &gt; </div>';
				}
			}
			$item_prop_content++;
			$str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="'. get_category_link($cat->term_id). '" itemprop="item"><span itemprop="name">'. $cat-> cat_name . '</span></a><meta itemprop="position" content="'.strval($item_prop_content).'" /></div>';
		} elseif(is_page()){
			$post_name = $post->post_title;
			if($post->post_parent != 0 ){
				$ancestors = array_reverse(get_post_ancestors( $post->ID ));
				foreach($ancestors as $ancestor){
					$item_prop_content++;
					$str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="'. get_permalink($ancestor).'" itemprop="item"><span itemprop="name">'. get_the_title($ancestor) .'</span></a><meta itemprop="position" content="'.strval($item_prop_content).'" /></div>';
				}
			}
			$item_prop_content++;
			$str.=' <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">'.$post_name.'</span><meta itemprop="position" content="'.strval($item_prop_content).'" /></div>';
		} elseif(is_single()){
			$categories = get_the_category($post->ID);
			$cat = $categories[0];
			$post_name = $post->post_title;
			if($cat->parent != 0){
				$ancestors = array_reverse(get_ancestors( $cat->cat_ID, 'category' ));
				foreach($ancestors as $ancestor){
					$item_prop_content++;
					$str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="'. get_category_link($ancestor).'" itemprop="item"><span itemprop="name">'. get_cat_name($ancestor). '</span></a><meta itemprop="position" content="'.strval($item_prop_content).'" /> &gt; </div>';
				}
			}
			$item_prop_content++;
			$str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="'. get_category_link($cat->term_id). '" itemprop="item"><span itemprop="name">'. $cat->cat_name . '</span></a><meta itemprop="position" content="'.strval($item_prop_content).'" /></div> > ';
			$item_prop_content++;
			$str.='<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">'.$post_name.'</span><meta itemprop="position" content="'.strval($item_prop_content).'" /></div>';
		} else{
			$str.='<div>'. wp_title('', false) .'</div>';
		}
	}
	echo $str; 
}




function img_dir() {
	$img_dir = get_template_directory_uri().'/assets/img/';
	return $img_dir;
}

function download_dir() {
	$download_dir = get_template_directory_uri().'/assets/download/';
	return $download_dir;
}

function no_img() {
	return get_template_directory_uri().'/assets/img/blog_noimage.png';
}

function ig_link() {
	$ig_link = 'https://www.instagram.com/webitworks';
	return $ig_link;
}

function fb_link() {
	$fb_link = 'https://www.facebook.com/webitworks.jp';
	return $fb_link;
}

function tw_link() {
	$tw_link = 'https://twitter.com/pirokinchos';
	return $tw_link;
}

function mid_t() {
	return 'medium';
}
function l_t() {
	return 'large';
}



function bdy_cls() {
	$bdy_cls = "";

	if( is_home() || is_front_page()){
		$bdy_cls = "home";
	} 
	
	if(is_page() && !(is_home() || is_front_page())) {
		$bdy_cls = "page";
	}

	if(is_category() || is_archive() || is_single() || is_search()) {
		$bdy_cls = "page post";
	}

	return $bdy_cls;
}




function add_index($content){
    if (is_single()) {
        $pattern = '/<h[1-6]>(.+?)<\/h[1-6]>/s';
        preg_match_all($pattern, $content, $elements, PREG_SET_ORDER);

        if (count($elements) >= 1) {
            $toc = '';
            $i = 0;
            $currentlevel = 0;
            $id = 'chapter-';

            foreach ($elements as $element) {
                $id .= $i + 1;
                $replace_title = preg_replace('/<(h[1-6])>(.+?)<\/(h[1-6])>/s', '<$1 id="' . $id . '">$2</$3>', $element[0]);
                $content = str_replace($element[0], $replace_title, $content);

                if (strpos($element[0], '<h2') !== false) {
                    $level = 1;
                } elseif (strpos($element[0], '<h3') !== false) {
                    $level = 2;
                } elseif (strpos($element[0], '<h4') !== false) {
                    $level = 3;
                } elseif (strpos($element[0], '<h5') !== false) {
                    $level = 4;
                } elseif (strpos($element[0], '<h6') !== false) {
                    $level = 5;
                }

                while ($currentlevel < $level) {
                    if ($currentlevel === 0) {
                        $toc .= '<ol class="index__list">';
                    } else {
                        $toc .= '<ol class="index__list_child">';
                    }
                    $currentlevel++;
                }

                while ($currentlevel > $level) {
                    $toc .= '</li></ol>';
                    $currentlevel--;
                }

                // 目次の項目で使用する要素を指定
                $toc .= '<li class="index__item"><a href="#' . $id . '" class="index__link">' . $element[1] . '</a>';
                $i++;
                $id = 'chapter-';
            } // foreach

            // 目次の最後の項目をどの要素から作成したかによりタグの閉じ方を変更
            while ($currentlevel > 0) {
                $toc .= '</li></ol>';
                $currentlevel--;
            }

            $index = '<div class="single__index index" id="toc"><div class="index__title">目次</div>' . $toc . '</div>';
            $h2 = '/<h2.*?>/i';

            if (preg_match($h2, $content, $h2s)) {
            $content = preg_replace($h2, $index . $h2s[0], $content, 1);
            }
        }
    }
    return $content;
}
add_filter('the_content', 'add_index');


function register_my_menus() { 
	register_nav_menus( array( 
		'header-nav' => 'ヘッダーナビ',
		'footer-sitemap-left'  => 'フッターサイトマップ左',
		'footer-sitemap-right'  => 'フッターサイトマップ右',
	) );
}
add_action( 'after_setup_theme', 'register_my_menus' );

