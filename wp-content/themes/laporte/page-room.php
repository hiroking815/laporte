<?php get_header(); ?>

    <?php
    global $post;
    $slug = $post->post_name;
    ?>

    <section class="common_content">
        <div class="common_content_wrapper">
            <div class="section_ttl">
                <h1><?php echo get_the_title(); ?></h1>
                <span class="sub_ttl"><?php echo ucfirst($slug); ?></span>
            </div>
            <div class="common_content_container">
                <p class="center sp_left">
                    ラ・ポルテは全15室全てが異なるエレガントなデザインのお部屋が揃い、 <br>
                    何度来ても楽しめる！ <br>
                    電話でのご予約が可能です。詳しくはホテルにご確認ください。 <br>
                    TEL <a href="tel:0773582226">0773-58-2226</a><br>
                    （ご予約いただきましたお客さまには素敵なプレゼントをご用意させていただいております。）
                </p> 
            </div>
        </div>
    </section>
    <section class="common_content">
        <?php 
        $type_a_list = [
            "102", "103", "105", "106", "107", "108", "110", "111",
        ]; 

        $type_b_list = [
            "101", "112", "113", "115", "116", "117",
        ]; 

        $type_c_list = [
            "118",
        ]; 
        ?>
        <div class="common_content_wrapper">
            <div class="section_ttl noborder">
                <h2>Type A</h2>
            </div>
            <div class="common_content_container dis_fl three">
                <?php foreach($type_a_list as $type_a):
                ?>
                    <div class="item w_300 sp_w_100per mb_20">
                        <a class="room_img" href="<?php echo img_dir().'room/room_'.$type_a.'.png'; ?>"  target="_blank" rel="noopener noreferrer">
                            <img src="<?php echo img_dir().'room/room_'.$type_a.'.png'; ?>">
                            <span class="room_number"><?php echo $type_a; ?></span>
                        </a>
                    </div>
                <?php 
                endforeach; ?>
                
            </div>
        </div>
    </section>

    <section class="common_content">

        <div class="common_content_wrapper">
            <div class="section_ttl noborder">
                <h2>Type B</h2>
            </div>
            <div class="common_content_container dis_fl three">

                <?php foreach($type_b_list as $type_b):
                ?>
                    <div class="item w_300 sp_w_100per mb_20">
                        <a class="room_img" href="<?php echo img_dir().'room/room_'.$type_b.'.png'; ?>"  target="_blank" rel="noopener noreferrer">
                            <img src="<?php echo img_dir().'room/room_'.$type_b.'.png'; ?>">
                            <span class="room_number"><?php echo $type_b; ?></span>
                        </a>
                    </div>
                <?php 
                endforeach; ?>
                
            </div>
        </div>
    </section>

    <section class="common_content">

        <div class="common_content_wrapper">
            <div class="section_ttl noborder">
                <h2>Type C</h2>
            </div>
            <div class="common_content_container dis_fl cen">


                <?php foreach($type_c_list as $type_c):
                ?>
                    <div class="item w_300 sp_w_100per mb_20">
                        <a class="room_img" href="<?php echo img_dir().'room/room_'.$type_c.'.png'; ?>"  target="_blank" rel="noopener noreferrer">
                            <img src="<?php echo img_dir().'room/room_'.$type_c.'.png'; ?>">
                            <span class="room_number"><?php echo $type_c; ?></span>
                        </a>
                    </div>
                <?php 
                endforeach; ?>
                
            </div>
        </div>
    </section>

    

<?php get_footer(); ?>
