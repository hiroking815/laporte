<?php get_header(); ?>

        <?php 

        $s = urldecode(get_search_query());
        ?>
        <section class="normal center">
            <div class="normal_wrapper">
                <h1>「<?php echo $s; ?>」の検索結果</h1>
            </div>
        </section>

        <section class="posts">
            <div class="post_inner inner">
                <div class="archive_items">
                    <?php 

                    if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
                    elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
                    else { $paged = 1; }  
                    $s_args = array(
                        'paged' => $paged,
                        'post_type' => 'post', 
                        'posts_per_page' => 10,
                        's' => $s
                    );

                    $s_query = new WP_Query( $s_args );
                    if ( $s_query->have_posts() ) : ?>
                        <?php 
                        while ( $s_query->have_posts() ) : 
                            $s_query->the_post(); 
                            $cat = get_the_category();
                            $cat_name = $cat[0]->cat_name; 
                            $post_date = get_the_date('Y.m.d'); 

                            $thumbnail_pass = "";
                            if(has_post_thumbnail()):
                                $thumbnail_pass = get_the_post_thumbnail();
                            else:
                                $thumbnail_pass = img_dir().'blog_noimage.png';
                            endif;

                            $content = "";
                            if(get_the_excerpt()):
                                $content = get_the_excerpt();
                            else:
                                $content = get_the_content();
                            endif; ?>
                            <a href="<?php echo get_the_permalink(); ?>" class="archive_item">

                                <img class="thumbnail" src="<?php echo $thumbnail_pass; ?>" alt="<?php the_title(); ?>">
                                <div class="archive_text">
                                    <p class="archive_ttl"><?php the_title(); ?></p>
                                    <div class="archive_content"><?php echo $content; ?></div>
                                    <div class="archive_item_info">
                                        <p class="archive_time"><i class="far fa-clock"></i><?php echo $post_date; ?></p>
                                        <p class="archive_category"><?php echo $cat_name ?></p>
                                    </div>
                                    
                                </div>

                            </a>
                        <?php 
                        endwhile;
                        ?>

                        <div class="pagination">
                        <?php if( function_exists("the_pagination") ) the_pagination(); ?>
                        </div>
                    <?php 
                    else : ?>
                        <p>検索キーワードに該当する記事がありませんでした。</p>
                    <?php 
                    endif; 
                    wp_reset_postdata(); ?>

                    


                </div>
                

                <?php get_sidebar(); ?>
                
                
            </div>
        </section>

    
<?php get_footer(); ?>
