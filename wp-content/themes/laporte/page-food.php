<?php get_header(); ?>

    <?php
    global $post;
    $slug = $post->post_name;

    $foods = [
        img_dir().'food/food01.jpg',
        img_dir().'food/food02.jpg',
        img_dir().'food/food03.jpg',
        img_dir().'food/food04.jpg',
        img_dir().'food/food05.jpg',
        img_dir().'food/food06.jpg',
        img_dir().'food/food07.jpg',
    ];


    ?>
    <section class="common_content">
        <div class="common_content_wrapper">
            <div class="section_ttl">
                <h1><?php echo get_the_title(); ?></h1>
                <span class="sub_ttl"><?php echo ucfirst($slug); ?></span>
            </div>
            <div class="common_content_container">
                <p class="center mb_40">掲載の料金は全て税込価格です。</p>
                <div class="dis_fl">
                    <?php 
                    foreach($foods as $food): ?>
                        <a href="<?php echo $food; ?>" target="_blank" class="food_item"><img src="<?php echo $food; ?>"></a>
                    <?php 
                    endforeach; ?>

                </div>
            </div>
        </div>
    </section>

    

<?php get_footer(); ?>
