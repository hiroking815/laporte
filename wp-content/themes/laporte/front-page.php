<?php get_header(); ?>


            <section class="main_visual">
                <div class="main_copy">
                    <p class="main_l_lttr"><?php echo get_bloginfo(); ?></p>
                </div>
            </section>
            <section class="common_content">
                <div class="common_content_wrapper">
                    <div class="common_content_container">
                        <p class="center sp_left">
                            2010年12月に福知山にニューオープンしたラ・ポルテ。<br>
                            豊かな自然に囲まれた恵まれた環境に佇む隠れ家的ホテル。<br>
                            全室フルリニューアルされたお部屋は露天風呂を備えた特別室や<br>
                            鮮やかなグラフィックが印象的なデザイナーズルームなどバリエーション豊富なラインナップ！ <br>
                            さあ、いち早くこのニューオープンホテルを遊びつくそう！
                        </p> 
                    </div>
                </div>
            </section>

            <section class="common_content">
                <div class="common_content_wrapper">
                    <div class="section_ttl">
                        <h2>お部屋</h2>
                        <span class="sub_ttl">Rooms</span>
                    </div>
                    <div class="common_content_container dis_fl bet">
                        <div class="w_400 sp_w_100per mb_20">
                            <img src="<?php echo img_dir().'room/room_101.png'; ?>">
                        </div>
                        <div class="w_550 sp_w_100per pt_20">
                            <p>
                            全15室全てがエレガントな装いで異なるデザインのお部屋が揃い何度きても楽しめる！<br>
                            またリーズナブルな料金システムも魅力。<br>
                            ショートタイムや最大10時間利用できるサービスタイムまで利用者のニーズにマッチしたシステムを選べる！
                            </p>
                            <a href="/room" class="cv_link">お部屋を見る<i class="fas fa-caret-right"></i></a>
                        </div> 
                    </div>
                </div>
            </section>

            <section class="common_content">
                <div class="common_content_wrapper">
                    <div class="section_ttl">
                        <h2>プラン・料金</h2>
                        <span class="sub_ttl">Plan / Price</span>
                    </div>
                    <div class="common_content_container dis_fl bet">
                        <div class="w_550 sp_w_100per sp_order_1 pt_20">
                            <p>
                            ラ・ポルテ福知山では、目的に応じたプラン・料金をご用意しております。<br>
                            プランや時間ごとの料金についての詳細はこちら。
                            </p>
                            <a href="/price" class="cv_link">料金を見る<i class="fas fa-caret-right"></i></a>
                        </div> 

                        <div class="w_400 sp_w_100per mb_20 sp_order_0">
                            <img src="<?php echo img_dir().'common/plan_price.jpg'; ?>">
                        </div>
                    </div>
                </div>
            </section>

            <section class="common_content">
                <div class="common_content_wrapper">
                    <div class="section_ttl">
                        <h2>予約</h2>
                        <span class="sub_ttl">Reservation</span>
                    </div>
                    <div class="common_content_container dis_fl bet">

                        <div class="w_400 sp_w_100per mb_20">
                            <img src="<?php echo img_dir().'common/reservation.jpg'; ?>">
                        </div>

                        <div class="w_550 sp_w_100per pt_20">
                            <p>
                            Webサービス「カップルズ」にて気軽にホテルの予約ができます。<br>
                            予約は以下のリンクより。
                            </p>
                            <a href="https://couples.jp/hotel-details/2300" class="cv_link" target="_blank" rel="noopener noreferrer">予約する<i class="fas fa-external-link-alt"></i></a>
                        </div> 

                    </div>
                </div>
            </section>

            <section class="common_content">
                <div class="common_content_wrapper">
                    <div class="section_ttl">
                        <h2>ラ・ポルテ福知山からのお知らせ</h2>
                        <span class="sub_ttl">Infomation</span>
                    </div>
                    <div class="common_content_container dis_fl bet">

                        <div class="w_300 sp_w_100per sp_mb_20">
                            <a href="<?php echo img_dir().'../pdf/coupon_laporte.pdf'; ?>" target="_blank" rel="noopener noreferrer"><img src="<?php echo img_dir().'common/banner_coupon.png'; ?>"></a>
                            <p class="center">クーポン</p>
                        </div>

                        <div class="w_300 sp_w_100per sp_mb_20">
                            <a href="member"><img src="<?php echo img_dir().'common/banner_member.png'; ?>"></a>
                            <p class="center">メンバーズカード</p>
                        </div>

                        <div class="w_300 sp_w_100per sp_mb_20">
                            <a href="/food"><img src="<?php echo img_dir().'common/banner_food.png'; ?>"></a>
                            <p class="center">フードメニュー</p>
                        </div>

                    </div>
                </div>
            </section>


            <section class="common_content">
                <div class="common_content_wrapper">
                    <div class="section_ttl">
                        <h2>アクセス</h2>
                        <span class="sub_ttl">Access</span>
                    </div>
                    <div class="common_content_container dis_fl bet">

                        <div class="w_400 sp_w_100per map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3260.255608300178!2d135.28021961524578!3d35.200101580309266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60001614f1905b8f%3A0x1e0c1da53a4b4316!2z44Op44O744Od44Or44OGIOemj-efpeWxseW6lw!5e0!3m2!1sja!2sjp!4v1636725106671!5m2!1sja!2sjp" width="600" height="360" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>

                        <div class="w_550 sp_w_100per">
                            <ul>
                                <li>舞鶴若狭自動車道「福知山IC」から国道9号線で京都方面へ。 三和町役場より京都方面に車で10分。</li>
                                <li>須地「やまがた屋」ドライブインより福知山方面へ車で10分。</li>
                            </ul>
                            〒620-1421 京都府福知山市三和町大身348<br>
                            TEL. <a href="tel:0773582226">0773-58-2226</a>
                            </p>
                            <a href="https://goo.gl/maps/S2Qwpn1FLvwppPYo7" class="cv_link" target="_blank" rel="noopener noreferrer">Googleマップで確認<i class="fas fa-external-link-alt"></i></a>
                        </div> 

                    </div>
                </div>
            </section>

    

<?php get_footer(); ?>
