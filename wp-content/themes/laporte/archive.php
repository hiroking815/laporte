<?php get_header(); ?>
    
        <?php 
        $this_cat = get_the_category();
        $this_cat_id = $this_cat[0]->term_id;
        $this_cat_slug = strtoupper($this_cat[0]->slug); 
        $this_cat_name = $this_cat[0]->name; ?>
        <section class="normal center">
            <div class="normal_wrapper">
                <h1><?php echo $this_cat_slug; ?></h1>
                <p><?php echo $this_cat_name; ?>記事一覧</p>
            </div>
        </section>

        <section class="posts">
            <div class="post_inner inner">
                <div class="archive_items">
                    <?php 
                    if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
                    elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
                    else { $paged = 1; }  
                                    
                    $args = array(
                        'paged' => $paged,
                        'post_type' => 'post', 
                        'posts_per_page' => 10,
                        'cat' => $this_cat_id
                    );
                    $the_query = new WP_Query( $args );
                    if ( $the_query->have_posts() ) : ?>
                        <?php 
                        while ( $the_query->have_posts() ) : 
                            $the_query->the_post(); 
                            $cat = get_the_category();
                            $cat_name = $cat[0]->cat_name; 
                            $post_date = get_the_date('Y.m.d'); 

                            $thumbnail_pass = "";
                            if(has_post_thumbnail()):
                                $thumbnail_pass = get_the_post_thumbnail_url();
                            else:
                                $thumbnail_pass = no_img();
                            endif;

                            $content = "";
                            if(get_the_excerpt()):
                                $content = get_the_excerpt();
                            else:
                                $content = get_the_content();
                            endif; ?>
                            <a href="<?php echo get_the_permalink(); ?>" class="archive_item">

                                <img class="thumbnail" src="<?php echo $thumbnail_pass; ?>" alt="<?php the_title(); ?>">
                                <div class="archive_text">
                                    <p class="archive_ttl"><?php the_title(); ?></p>
                                    <div class="archive_content"><?php echo $content; ?></div>
                                    <div class="archive_item_info">
                                        <p class="archive_time"><i class="far fa-clock"></i><?php echo $post_date; ?></p>
                                        <p class="archive_category"><?php echo $cat_name ?></p>
                                    </div>
                                    
                                </div>

                            </a>
                        <?php 
                        endwhile;
                        ?>

                        <div class="pagination">
                            <?php
                                // $bignum = 999999999;
                                // $paginate_args = array(
                                //     'base'         => str_replace( $bignum, '%#%', '?page=%#%' ),
                                //     // 'base'         => '%_%',
                                //     'format'       => '',
                                //     'current'      => max(0, $paged),
                                //     'total'        => $the_query->max_num_pages,
                                //     'prev_text'    => '&lt;',
                                //     'next_text'    => '&gt;',
                                //     'type'         => 'list',
                                //     'end_size'     => 1,
                                //     'mid_size'     => 2
                                // );

                                // if ( $the_query->max_num_pages <= 1 ) {
                                //     return;
                                // }
                                // echo paginate_links( $paginate_args );
                                the_pagination()
                            ?>

                        </div>
                    <?php 
                    else : ?>
                            <p>記事はありません。</p>
                    <?php 
                    endif; 
                    wp_reset_postdata(); ?> 

                    


                </div>
                

                <?php get_sidebar(); ?>
                
                
            </div>
        </section>

    
<?php get_footer(); ?>
