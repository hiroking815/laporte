<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link https://ja.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'kyotobrew_811wx' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'kyotobrew_oox5a' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', 's7n7efaquy' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'mysql7021.xserver.jp' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define( 'DB_COLLATE', '' );

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'g. ^tAs]j)bRif!<Sch.@Bsv.&7o$GGTs{P^lHoAyXZ.$W=dw2>a3l2A5re~bLQ;' );
define( 'SECURE_AUTH_KEY',  'PI<Xk2lZSOY)GQis{(Tnqzs&XXZd`cC(mEEBu6&V9>ZBx(2L=M(I;F9+E~y2#Td3' );
define( 'LOGGED_IN_KEY',    '-5X=Noutix,x2|n?<+YG2TbpJ+uRd8!AI]OdaU[soo^+o>1]=9~0]@jWzO(RXv:d' );
define( 'NONCE_KEY',        ')3>^hb+&K^ .tGUBvlCAAlPC:=c2)?V1o&6J=DV5>Wpw7*=]T?.33o{QK9JWNQig' );
define( 'AUTH_SALT',        'D !9<Vt}5Cj$G0ep6jf1Ap?<xKcRty4ElPUJlWL 1v*KL5F.<*&<>*hpMQJ$i0sF' );
define( 'SECURE_AUTH_SALT', '{~u|{O48?e[RDp}G%%3dkwRVd?pj^pYp.iLfg2J$aWqI=%WkZ_.NO>tQL7*t5l>w' );
define( 'LOGGED_IN_SALT',   'Ocw.u#[gqf>zKaTRR(GjQzR<0|1e8s;drnig:2](VY<;lOhXczD(P-t$Z+[{u1q8' );
define( 'NONCE_SALT',       '2+O,wyx4uSf]!:<-n!x4)j[</W&TbC<MdmI9Q@n#phf=RNF!fHr0-C*l%t-C=hQ/' );
define( 'WP_CACHE_KEY_SALT','2PHdVK5o+GpBA5B;Si9efuaQ,7t^gP{|?MKgXkY>5eN(3HteE+bBv1m1IYhNGYV]' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数についてはドキュメンテーションをご覧ください。
 *
 * @link https://ja.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* カスタム値は、この行と「編集が必要なのはここまでです」の行の間に追加してください。 */



/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
